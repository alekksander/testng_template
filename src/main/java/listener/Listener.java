package listener;

import org.apache.log4j.Logger;
import org.testng.*;
import setup.Utils;
import tests.BaseTest;


public class Listener implements ITestListener, IInvokedMethodListener2 {

    private static final Logger LOGGER = Logger.getLogger(Listener.class.getName());

    @Override
    public void onTestStart(ITestResult result) {
    }

    @Override
    public void onTestSuccess(ITestResult result) {
    }

    @Override
    public void onTestFailure(ITestResult result) {

    }

    @Override
    public void onTestSkipped(ITestResult result) {
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    @Override
    public void onStart(ITestContext context) {
    }

    @Override
    public void onFinish(ITestContext context) {
    }

    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult, ITestContext iTestContext) {
    }

    @Override
    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult result, ITestContext iTestContext) {

        //запись в тест реил
        if(iInvokedMethod.isTestMethod()){
            //скриншот для тест реил (копируем в хранилище скриншотов) и привязываем его к результату теста

            if(!result.isSuccess()){
                Object currentClass = result.getInstance();
                Utils utils = ((BaseTest) currentClass).getUtils();
                if(utils!=null){
                    utils.screenshot(result); //сделать скриншот + копирование в хранилизе скринов
                }
            }
        }
    }

    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
    }

    @Override
    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
    }
}