package log;

import java.io.IOException;
import java.util.logging.LogManager;

/**
 * Created by sergey.lugovskoi
 */
public class LogConfigLoader {

    private static LogConfigLoader instance;

    /**
     * синглтон, при создании загружает настройки логгеров
     * @return возвращает инстанс этого класса
     */
    public static LogConfigLoader getInstance() {
        LogConfigLoader localInstance = instance;
        if (localInstance == null) {
            synchronized (LogConfigLoader.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new LogConfigLoader();
                }

            }
        }
        return localInstance;
    }

    private LogConfigLoader() {
        try {
            //java.util.logging
            LogManager.getLogManager().readConfiguration(LogConfigLoader.class.getClassLoader().getResource("logging.properties").openStream());
            System.out.println("java.util.logging level set to: "+LogManager.getLogManager().getProperty(".level"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
