package log;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Aspect
public class LogStep {
    private static final Logger LOGGER = Logger.getLogger(LogStep.class.getName());

    @Pointcut("execution(@Log * *(..))")
    public void isAnnotated() {
    }

    @Around("isAnnotated()")
    public Object logMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();    //таймер выполнения метода
        Object retVal = joinPoint.proceed();    //выполнение метода
        String methodName = joinPoint.getSignature().getName(); //имя метода
        Object[] args_ = joinPoint.getArgs(); //аргументы метода
        Object obj = joinPoint.getTarget();  //объект метода
        //формат времени для логирования
        long time = stopWatch.getTime();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(time);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(time);
        String elapsed = String.format("%d min, %d sec, %d msec",
                minutes,
                seconds - TimeUnit.MINUTES.toSeconds(minutes),
                time - (minutes*60*1000) - (seconds * 1000)
        );

        LOGGER.info("STEP " + methodName + " " + Arrays.toString(args_) + " ||| time ||| "+elapsed + "||| class ||| "+obj.getClass().getSimpleName());
        stopWatch.stop();       //стоп таймер
        return retVal;
    }
}
