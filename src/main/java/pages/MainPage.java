package pages;

import log.Log;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.annotations.Timeout;
import ru.yandex.qatools.htmlelements.element.TextInput;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import setup.ElementsHelper;

public class MainPage implements Page{

    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(new HtmlElementDecorator(driver), this);
        this.driver = driver;
    }

    @Timeout(5)
    @SuppressWarnings("unused")
    @FindBy(xpath = "//input[@id='lst-ib']")
    private TextInput searchField;

    @Timeout(5)
    @SuppressWarnings("unused")
    @FindBy(xpath = "//div[@id='res']")
    private WebElement results;


    @Step("поиск на странице {0}")
    @Log
    public void search(String searchtext){
        searchField.clear();
        searchField.sendKeys(searchtext);
        searchField.sendKeys(Keys.ENTER);
        ElementsHelper.waitElement(results);
    }


    public WebDriver getDriver() {
        return driver;
    }

    @Override
    @Step
    public void open() {

    }
}
