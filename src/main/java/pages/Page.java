package pages;

import org.openqa.selenium.WebDriver;

public interface Page {

    void open();
    WebDriver getDriver();
}
