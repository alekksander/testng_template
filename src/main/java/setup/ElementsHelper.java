package setup;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.htmlelements.element.TypifiedElement;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static ru.yandex.qatools.htmlelements.matchers.MatcherDecorators.should;
import static ru.yandex.qatools.htmlelements.matchers.common.IsElementDisplayedMatcher.isDisplayed;
import static ru.yandex.qatools.htmlelements.matchers.decorators.TimeoutWaiter.timeoutHasExpired;

public class ElementsHelper {

    private static final int timeout = 10000;

    @Step
    public static TypifiedElement waitElement(TypifiedElement elem){
        assertThat(elem.getWrappedElement(), should(isDisplayed()).whileWaitingUntil(timeoutHasExpired(timeout)));
        return elem;
    }

    @Step("Ожидание доступности элемента {1} {2} сек")
    public static void waitForStaleness(WebDriver driver, WebElement elem, int timeOut) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut, 500); //100
        wait.until(ExpectedConditions.elementToBeClickable(elem));
    }

    @Step("Ожидание доступности элемента {1} {2} сек")
    public static void waitForStaleness(WebDriver driver, TypifiedElement elem, int timeOut) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut, 500); //100
        wait.until(ExpectedConditions.elementToBeClickable(elem.getWrappedElement()));
    }

    @Step
    public static WebElement waitElementNotVisible(WebElement elem){
        assertThat(elem, should(not(isDisplayed())).whileWaitingUntil(timeoutHasExpired(timeout)));
        return elem;
    }

    @Step
    public static WebElement waitElement(WebElement elem){
        assertThat(elem, should(isDisplayed()).whileWaitingUntil(timeoutHasExpired(timeout)));
        return elem;
    }

    @Step
    public static TypifiedElement waitElementNotVisible(TypifiedElement elem){
        assertThat(elem.getWrappedElement(), should(not(isDisplayed())).whileWaitingUntil(timeoutHasExpired(timeout)));
        return elem;
    }

    @Step
    public static void checkNotVisible(TypifiedElement elem){
        assertThat(elem.getWrappedElement().isDisplayed(),is(false));
    }

    @Step("проверка видимости элемента {0}")
    public static boolean isElementVisible(WebElement elem) {
        try {
            elem.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @Step("проверка видимости элемента {0}")
    public static boolean isElementVisible(TypifiedElement elem) {
        try {
            elem.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}