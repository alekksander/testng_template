package setup;

import entities.User;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ITestResult;
import ru.yandex.qatools.allure.annotations.Attachment;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;


public class Utils {

    private WebDriver driver;
    public Utils(WebDriver driver) {
        this.driver = driver;
    }
    private static final Logger LOGGER = Logger.getLogger(Utils.class.getName());


    //скриншот для не тестового метода
    @Attachment(value = "screenshot", type = "image/png")
    public byte[] takeScreenShoot() {
        try {
            WebDriver augmentedDriver = new Augmenter().augment(driver);
            File screenshot;
            try{
                screenshot = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
            }
            catch (UnhandledAlertException a){
                driver.switchTo().alert().accept();
                screenshot = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
            }
            String nameScreenshot = getFileName(DataGenerator.getRandomNameWithDate());
            String path = getPath(nameScreenshot);
            File newScreen = new File(path);
            FileUtils.copyFile(screenshot, newScreen);
            BufferedImage bImage = ImageIO.read(screenshot);
            byte[] imageInByteArray;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bImage, "png", baos);
            imageInByteArray = baos.toByteArray();
            baos.flush();
            return imageInByteArray;
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
            return null;
        }
    }

    //скриншот для метода с аннотацией @Test
    @Attachment(value = "screenshot", type = "image/png")
    private byte[] takeScreenShoot(ITestResult result) {
        try {
            WebDriver augmentedDriver = new Augmenter().augment(driver);
            File screenshot;

            //если несколько окон то скриним последнее и потом переключаемся обратно к первому
            String oldHandle = driver.getWindowHandle();
            Set<String> handles = driver.getWindowHandles();
            if(handles.size()>1){
                handles.stream().forEach(h->driver.switchTo().window(h));
            }
            try {
                screenshot = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
            } catch (UnhandledAlertException a) {
                driver.switchTo().alert().accept();
                screenshot = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
            }
            if(handles.size()>1)driver.switchTo().window(oldHandle);
            String nameScreenshot = result.getTestClass().getClass().getSimpleName() + "_" + getFileName(DataGenerator.getRandomIntNumber(1000000,2000000));
            String path = getPath(nameScreenshot);
            File newScreen = new File(path);
            FileUtils.copyFile(screenshot, newScreen);

            BufferedImage bImage = ImageIO.read(screenshot);
            byte[] imageInByteArray;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bImage, "png", baos);
            imageInByteArray = baos.toByteArray();
            baos.flush();
            return imageInByteArray;
        } catch (IOException | NullPointerException ex) {
            LOGGER.error(ex.getMessage());
            return null;
        }
    }

    private static String getPath(String nameTest) throws IOException {
        File directory = new File(".");
        return directory.getCanonicalPath() + "/target/allure-results/" + nameTest;
    }

    private static String getFileName(String nameTest) {
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy_HH.mm.ss");
        Date date = new Date();
        return dateFormat.format(date) + "_" + nameTest + ".png";
    }

    //(копируем в хранилище скриншотов - адрес в pom.xml или папка screenshots внутри проекта)
    private static void copyScreenToStorage(byte[] screen, ITestResult result){
        try {
            InetAddress addr;
            String storageDir = System.getProperty("screenshotsStorage");
            addr = InetAddress.getLocalHost();
            String hostname;
            hostname = addr.getHostName();
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(screen));
            String screenshotStoragePath = storageDir==null?"screenshots\\":storageDir;
            String browserVersion = DriverSetup.getBrowserVersion();
            storageDir = storageDir==null?"screenshots\\":storageDir;
            File dir = new File(storageDir);
            if(!dir.exists())dir.mkdir();
            File outputfile = new File(screenshotStoragePath + result.getTestClass().getRealClass().getSimpleName() +" "+hostname + " "+ browserVersion +" "+  result.getMethod().getMethodName() +"_" + Utils.getFileName(DataGenerator.getRandomIntNumber(1000000,2000000)));
            ImageIO.write(img, "png", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
         }
    }

    /**
     *
     * считывает логины пользователей из указанного файйла в очередь
     */
    static ConcurrentLinkedQueue<User> readFileToQueue(String fileName){

        ConcurrentLinkedQueue<User> queue = new ConcurrentLinkedQueue<User>();
        File file = new File(Utils.class.getClassLoader().getResource(fileName).getFile());
        try (FileInputStream stream = new FileInputStream(file);
             InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
             BufferedReader config = new BufferedReader(reader)) {
            String str1;
            while ((str1 = config.readLine()) != null) {
                User user = new User(str1);
                queue.add(user);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return queue;
    }

    /**
     * метод сделает скриншот и копирует его в хранилизе скринов
     *
     * @param result текущий тест
     * @return скриншот
     */
    public byte[] screenshot(ITestResult result, String...errorNames){
        byte[] screen = takeScreenShoot(result);
        if(screen!=null && result!=null)Utils.copyScreenToStorage(screen, result);

        return screen;
    }
}
