package setup;

import entities.User;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UserKeeper {

    private static volatile UserKeeper instance;
    private static volatile ConcurrentLinkedQueue<User> freeUsers;
    private static final Logger LOGGER = Logger.getLogger(UserKeeper.class.getName());


    private UserKeeper() throws IOException {

        freeUsers = Utils.readFileToQueue("logins.txt");
        LOGGER.info("USERS FOR TESTS: " + freeUsers);
    }

    public static UserKeeper getInstance() {
        UserKeeper localInstance = instance;
        if (localInstance == null) {
            synchronized (UserKeeper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    try {
                        instance = localInstance = new UserKeeper();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return localInstance;
    }

    //взять юзера из очереди свободных
    public User takeUser() {
        User user = freeUsers.poll();
        LOGGER.info("TAKE USER " + user);
        return user;
    }

    //освободить юзера (добавить в очередь свободных юзеров на выдачу тестам)
    public void freeUser(User user) {

        if (!freeUsers.contains(user)) {
            freeUsers.add(user);
            LOGGER.info("QUEUE " + freeUsers);
            LOGGER.info("FREE USER " + user);
        }
    }

}

