package setup;

import javafx.util.Pair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class DataGenerator {

    private static final List<String> nouns = new ArrayList<>();
    private static final List<String> adjectives = new ArrayList<>();

    static {
        URL url1 = DataGenerator.class.getClassLoader().getResource("dictionaries/nouns.ru");
        URL url2 = DataGenerator.class.getClassLoader().getResource("dictionaries/adjectives.ru");
        File nouns = new File(url1.getPath());
        File adjectives = new File(url2.getPath());

        try (Scanner sc = new Scanner(new FileInputStream(nouns), "UTF-8");
             Scanner sc2 = new Scanner(new FileInputStream(adjectives), "UTF-8")) {

            while(sc.hasNextLine())
                DataGenerator.nouns.add(sc.nextLine());

            while(sc2.hasNextLine())
                DataGenerator.adjectives.add(sc2.nextLine());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    public static String getCurrentDate(){
        Date j = new Date();
        j.getTime();
        DateFormat df = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
        return df.format(j);
    }

    public enum Gender {
        male,
        female,
        neutral
    }
   
    public static String getRandomIntNumber(){
        Random rand = new Random();
        int min = 100;
        int max = 10000;
        return Integer.toString(rand.nextInt((max - min)));
    }

    public static String getRandomIntNumber(int start, int stop){
        Random rand = new Random();
        return Integer.toString(rand.nextInt((stop - start)));
    }

    //Случайная дата рождения в интервале 01.01.1951-31.12.2010
    public static String randomBirthDate(){
        Random rand = new Random();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, rand.nextInt(60) + 1950);
        calendar.set(Calendar.DAY_OF_YEAR, rand.nextInt(364) + 1);
        return new SimpleDateFormat("dd.MM.yyyy").format(calendar.getTime());
    }

    public static String getRandomNameWithDate() {
        return getCurrentDate()+" "+getRandomIntNumber();
    }

    private static String[] maleEndings = {"б", "в", "г", "д", "ж", "з", "й", "к", "л", "м", "н", "п", "р", "с", "т", "ф", "х", "ц", "ч", "ш", "щ", "тель", "рь", "ень", "онок", "енок"};
    private static String[] femaleEndings = {"а", "я", "ия", "ь"};
    private static String[] neutralEndings = {"о", "е", "ё", "ие"};

    private static Gender genderOf(String word) {
        for (String e : maleEndings)
            if (word.endsWith(e))
                return Gender.male;

        for (String e : femaleEndings)
            if (word.endsWith(e))
                return Gender.female;

        for (String e : neutralEndings)
            if (word.endsWith(e))
                return Gender.neutral;

        return Gender.male;
    }

    public static String randomNoun() {
        return capitalizeFirstLetter(nouns.get(new Random().nextInt(nouns.size())));
    }
    public static String randomAdjective() {
        return capitalizeFirstLetter(adjectives.get(new Random().nextInt(adjectives.size())));
    }
    public static String randomAdjective(Gender gender) {
        return inflictAdjective(randomAdjective(), gender);
    }
    private static Pair<String,String> randomAdjNounPair() {
        String noun = capitalizeFirstLetter(randomNoun());
        String adj = capitalizeFirstLetter(randomAdjective(genderOf(noun)));
        return new Pair<>(adj,noun);
    }
    public static String randomAdjNoun() {
        Pair<String,String> pair = randomAdjNounPair();
        return pair.getKey() + " " + pair.getValue();
    }
    private static String inflictAdjective(String adj, Gender gender) {
        switch (gender) {
            case female:
                if (adj.endsWith("ый"))
                    return adj.replace("ый", "ая");
                else if (adj.endsWith("ой"))
                    return adj.replace("ой", "ая");
                else if (adj.endsWith("кий"))
                    return adj.replace("кий", "кая");
                else if (adj.endsWith("гий"))
                    return adj.replace("гий", "гая");
                else if (adj.endsWith("ий"))
                    return adj.replace("ий", "яя");
                else if (adj.endsWith("ов"))
                    return adj.replace("ов", "ова");
                else if (adj.endsWith("ев"))
                    return adj.replace("ев", "ева");

                return adj;
            case neutral:
                if (adj.endsWith("ый"))
                    return adj.replace("ый", "ое");
                else if (adj.endsWith("ой"))
                    return adj.replace("ой", "ое");
                else if (adj.endsWith("кий"))
                    return adj.replace("кий", "кое");
                else if (adj.endsWith("гий"))
                    return adj.replace("гий", "гое");
                else if (adj.endsWith("ий"))
                    return adj.replace("ий", "ее");
                else if (adj.endsWith("ов"))
                    return adj.replace("ов", "ово");
                else if (adj.endsWith("ев"))
                    return adj.replace("ев", "ево");

                return adj;
            default:
                return adj;
        }
    }
    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }
}