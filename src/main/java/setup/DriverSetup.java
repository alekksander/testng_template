package setup;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;
import java.util.logging.Level;

/**
 *
 * @author s.lugovskiy
 */
public class DriverSetup {
    private static String remoteUrl = System.getProperty("remoteUrl");
    private static Logger LOGGER = Logger.getLogger(DriverSetup.class.getName());
    private String browser;
    private WebDriver driver;
    private static String browserVersion;


    @Step("Инициализация драйвера")
    @Attachment
    public WebDriver getDriver(){
        WebDriver driver = null;
        browser = System.getProperty("driver");
        if(browser==null)browser="chrome";
        DesiredCapabilities capabilities;
        switch (browser) {
            case "phantomjs":
                capabilities = DesiredCapabilities.phantomjs();
                break;
            case "chrome":
                capabilities = DesiredCapabilities.chrome();
                break;
            case "firefox":
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("network.proxy.type", 0);
                capabilities = DesiredCapabilities.firefox();
                capabilities.setCapability(FirefoxDriver.PROFILE, profile);
                capabilities.setCapability("marionette", true);
                break;
            case "ie":
                DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
                ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                capabilities = ieCapabilities;
                break;
            default:
                capabilities = DesiredCapabilities.firefox();
                break;
        }
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.SEVERE);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        try {
            if(remoteUrl!=null){
                driver = new RemoteWebDriver(new URL(remoteUrl), capabilities);
            }
            else{
                driver = new RemoteWebDriver(new URL("http://127.0.0.1:4447/wd/hub"), capabilities);
            }

        } catch (MalformedURLException ex) {
            LOGGER.error(ex);
        }
        assert driver != null;
        //driver.manage().window().maximize();
        if(Objects.equals(browser, "chrome")){
            try {
                setChromeProperties(driver);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
        driver.manage().window().maximize();
        this.driver = driver;
        browserVersion = detectBrowserType();
        return driver;
    }

    /**
     * устанавливает место загрузки файлов в хроме
     * @throws InterruptedException
     * @throws IOException
     */
    private void setChromeProperties(WebDriver driver) throws InterruptedException, IOException {
        File tempDir = new File("saved_files");
        if(!tempDir.exists() & !tempDir.isDirectory()){
            tempDir.mkdir();
        }
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String tmpDirEscapedPath = tempDir.getCanonicalPath().replace("\\", "\\\\");
        driver.get("chrome://settings-frame");
        driver.findElement(By.id("advanced-settings-expander")).click();
        WebDriverWait wait = new WebDriverWait(driver, 10, 120); //100
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("downloadLocationPath")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("downloadLocationPath")));

        String prefId = "download.default_directory";
        js.executeScript(String.format("Preferences.setStringPref('%s', '%s', true)", prefId, tmpDirEscapedPath));
    }

    /**
     *
     * @return String версия браузера
     */
    private  String detectBrowserType() {
        String browser_version;
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browsername = cap.getBrowserName();
        if ("internet explorer".equalsIgnoreCase(browsername)) {
            String uAgent = (String) ((JavascriptExecutor) driver).executeScript("return navigator.userAgent;");
            System.out.println(uAgent);
            if (uAgent.contains("MSIE") && uAgent.contains("Windows")) {
                browser_version = uAgent.substring(uAgent.indexOf("MSIE")+5, uAgent.indexOf("Windows")-2);
            } else if (uAgent.contains("Trident/7.0")) {
                browser_version = "11.0";
            } else {
                browser_version = "0.0";
            }
        } else
        {
            browser_version = cap.getVersion();// .split(".")[0];
        }
        String browserversion = "";
        if(browser_version.length()>0)browserversion = browser_version.substring(0, browser_version.indexOf("."));
        return browsername + " " + browserversion;
    }

    public static String getBrowserVersion() {
        return browserVersion;
    }
}
