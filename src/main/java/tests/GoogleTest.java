package tests;

import org.testng.annotations.Test;
import pages.MainPage;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.io.IOException;

@Test
public class GoogleTest extends BaseTest{


    @Test
    @Features("Главная страница Гугл")
    @Stories("поиск")
    public void mainPageTest() throws IOException {

        MainPage page = new MainPage(driver);
        page.open();
        page.search("xyz");


    }

}
