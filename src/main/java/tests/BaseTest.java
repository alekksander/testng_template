package tests;

import entities.User;
import log.Log;
import log.LogConfigLoader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.DriverSetup;
import setup.UserKeeper;
import setup.Utils;

@Test
public class BaseTest {
    WebDriver driver;
    private User user;
    private Utils utils;
    private String baseUrl;


    @BeforeClass
    @Log
    public void initTest() {
        initDriver();   //открытие окна браузера для теста
        LogConfigLoader.getInstance();  //загрузка настроек логгеров
        baseUrl = System.getProperty("baseUrl");
        initUser(); //получение объекта юзера для дальнейшего использования
        utils = new Utils(driver);
        driver.get(baseUrl);
    }

    protected void initUser() {
        user = UserKeeper.getInstance().takeUser();
    }

    @Log
    protected void initDriver() {
        DriverSetup setup = new DriverSetup();
        driver = setup.getDriver();
    }

    @AfterClass
    @Log
    public void close() {
        //проверить на наличие алерта (с ним не закроется)
        try{driver.switchTo().alert().accept();}
        catch (WebDriverException ignored){}
        try{
            driver.close();
            driver.quit();
        }catch (WebDriverException ignored){

        }
        UserKeeper.getInstance().freeUser(user);
    }

    public Utils getUtils() {
        return utils;
    }

    public WebDriver getDriver() {
        return driver;
    }

}
